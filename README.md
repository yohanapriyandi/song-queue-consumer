Sebelum menggunakan proyek ini silahkan instal seluruh dependensi yang dibutuhkan dengan perintah:

**npm install** atau yang lebih pendek **npm i**

pastikan inatalasi berhasil apabila berhasil akan muncul folder/direktori *node_modules*

disini menggunakan database **postgre sql**, kemudian buat file .env dan masukan parameter berikut:

# node-postgres configuration

PGUSER=<user database anda>

PGHOST=localhost

PGPASSWORD=<password>

PGDATABASE=<nama database>

PGPORT=5432

# nodemailer SMTP authentication

MAIL_ADDRESS=<isi dengan email yang valid>

MAIL_PASSWORD=<isi dengan password yang valid>

# RABBITMQ SERVER

RABBITMQ_SERVER=<alamat server untuk rabbit mq>